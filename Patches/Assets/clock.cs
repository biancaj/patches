﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class clock : MonoBehaviour {

	public AudioClip ding;
	private float timeLapse = 30f;
	private float currentTime = 0f;
	private AudioSource clockSpeaker;
	// Use this for initialization

	void Awake()
	{
		clockSpeaker = this.GetComponent<AudioSource> ();

	}

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.time == currentTime + timeLapse) {
			clockSpeaker.PlayOneShot (ding);
			currentTime = Time.time;
		}

	}
}
