﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBlackout : MonoBehaviour
{
    public GameObject left_hand;
    public GameObject right_hand;
    public GameObject flashlight;
    public GameObject camera;
    public GameObject canvas;
    public GameObject clock;
    public int blackoutSeconds;
    public int activeObject;
    public AudioListener al;
    public AudioSource patchesAudio;
    public AudioClip mine;
    //public Transform[] upstairs;
    //public Transform[] downstairs;


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Patches")
        {
            Debug.Log(collision.transform.name);
            patchesAudio = collision.gameObject.GetComponent<AudioSource>();
            GameObject.Find("GameManager").GetComponent<SpawnPatches>().TeleportPatches();  // Teleport patches uppon collision
            StartCoroutine(MovementWait(collision.gameObject));                             // Start process in which player will blackout
        }
    }

    IEnumerator MovementWait(GameObject P)
    {
        P.GetComponent<Patches>().agent.isStopped = true;                   // Stop patches from moving after teleport
        gameObject.GetComponent<OVRPlayerController>().blackedout = true;   // Stop player from movement
        left_hand.SetActive(false);                                         // Player cant use left hand
        camera.SetActive(false);
        activeObject = (right_hand.activeSelf == true) ? 0 : 1;
        flashlight.SetActive(false);
        right_hand.SetActive(false);                                        // Player cant use right hand
        patchesAudio.clip = mine;
        patchesAudio.Play();
        canvas.SetActive(true);
        yield return new WaitForSeconds(patchesAudio.clip.length);
        al.enabled = false;                                          // Set blackout canvas to true, make visible


        yield return new WaitForSeconds(blackoutSeconds);                   // Wait for a cetain ammount of seconds

        P.GetComponent<Patches>().agent.isStopped = false;                  // Start patches movement
        gameObject.GetComponent<OVRPlayerController>().blackedout = false;  // Start player movement again
        left_hand.SetActive(true);                                          // Player can use left hand
        camera.SetActive(true);
        flashlight.SetActive(true);
        //if (activeObject==0)
        //{
        //    right_hand.SetActive(true);
        //}
        //else if(activeObject==1)
        //{
        //    flashlight.SetActive(true);
        //}
        al.enabled = true;
        //right_hand.SetActive(true);                                         // Player can use right hand
        canvas.SetActive(false);                                            // Set blackout canvas to true, make invisible

        clock.GetComponent<Clock_Time>().hourInterval += 1 ;

    }
}
