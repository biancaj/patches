﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SphereCastView : MonoBehaviour
{

    public GameObject currentHitObject; // To hold the current object being hit.
    public float sphereRadius;          // To set the radius of the sphere
    public float maxDistance;           // To hold the max distance of sphere
    public LayerMask layerMask;         // To hold a set of layer masks, to decide what the sphere cast can collide to or not

    private Vector3 origin;             // To origin where teh sphere should start
    private Vector3 direction;          // To hold direction in which ray is gonna be shot in 
    private float currentHitDistance;   // Distance to current object

    public GameObject gameManager;
    public Light spot;

    public bool continuePlaying;
    public bool coroutineEnd;

    public AudioSource parentsHitSound;
    public GameOver parentsHitCounter;

    public bool doorHit;

    // Use this for initialization
    void Start()
    {
        continuePlaying = false;
        coroutineEnd = true;
        parentsHitCounter = GameObject.FindGameObjectWithTag("Game Manager").GetComponent<GameOver>();
        doorHit = false;
    }

    // Update is called once per frame
    void Update()
    {
        origin = transform.position;    // Origin initialized to 
        direction = transform.forward;
        RaycastHit hit;

        if (Physics.SphereCast(origin, sphereRadius, direction, out hit, maxDistance, layerMask, QueryTriggerInteraction.UseGlobal) && spot.intensity > 0.2f)
        {
            currentHitObject = hit.transform.gameObject;
            currentHitDistance = hit.distance;

            if (currentHitObject.gameObject.tag == "ShyLight")
            {
                //Debug.Log("You've captured Elephant");
                if (gameManager.GetComponent<Enemy_Waypoints>() && coroutineEnd == true)
                {
                    Debug.Log("You've captured Elephant");
                    AudioSource audio = currentHitObject.gameObject.GetComponent<AudioSource>();
                    audio.clip = gameManager.GetComponent<Enemy_Waypoints>().elephantSources[0];
                    audio.Play();
                    StartCoroutine(RunFromLight());
                }
            }
            if (currentHitObject.gameObject.tag == "LightFreeze")
            {
                if (currentHitObject.gameObject.name == "Love Bear 1(Clone)")
                {
                    //Debug.Log("You've captured Bear");
                    if (gameManager.GetComponent<Enemy_Waypoints>())
                    {
                        gameManager.GetComponent<Enemy_Waypoints>().freeze_bearOne = true;
                    }
                }
                else if (currentHitObject.gameObject.name == "Love Bear 2(Clone)")
                {
                    //Debug.Log("You've captured Bear");
                    if (gameManager.GetComponent<Enemy_Waypoints>())
                    {
                        gameManager.GetComponent<Enemy_Waypoints>().freeze_bearTwo = true;
                    }
                }
            }
            else if (gameManager.GetComponent<Enemy_Waypoints>().freeze_bearOne == true && currentHitObject.gameObject.tag != "LightFreeze")
            {
                if (gameManager.GetComponent<Enemy_Waypoints>())
                {
                    gameManager.GetComponent<Enemy_Waypoints>().freeze_bearOne = false;
                }
            }

            else if (gameManager.GetComponent<Enemy_Waypoints>().freeze_bearTwo == true && currentHitObject.gameObject.tag != "LightFreeze")
            {
                if (gameManager.GetComponent<Enemy_Waypoints>())
                {
                    gameManager.GetComponent<Enemy_Waypoints>().freeze_bearTwo = false;
                }
            }

            if (currentHitObject.gameObject.tag == "Parents Door" && doorHit == false)
            {

                if (parentsHitCounter.wakeUpCounter == 1 && doorHit == false && gameManager.GetComponent<GameOver>().wakeUpParents != true)
                {
                    
                    parentsHitSound.clip = gameManager.GetComponent<GameOver>().GetAudioClip(gameManager.GetComponent<GameOver>().parentsAudioEnd);
                    parentsHitSound.Play();
                    //if (!parentsHitSound.isPlaying)
                    //{
                        gameManager.GetComponent<GameOver>().wakeUpParents = true;
                    //}
                }
                else if(doorHit == false)
                {
                    //parentsHitCounter++;
                    parentsHitSound.clip = gameManager.GetComponent<GameOver>().GetAudioClip(gameManager.GetComponent<GameOver>().parentsAudioWarning);
                    parentsHitSound.Play();
                }
                doorHit = true;
            }
            if (currentHitObject.gameObject.tag != "Parents Door" && doorHit == true && gameManager.GetComponent<GameOver>().wakeUpParents != true)
            {
                parentsHitCounter.wakeUpCounter++;
                doorHit = false;
            }

            if (currentHitObject.gameObject.tag == "Patches" && gameManager.GetComponent<SpawnPatches>() && gameManager.GetComponent<SpawnPatches>().teleport == false)
            {
                if (gameManager.GetComponent<SpawnPatches>())
                {
                    gameManager.GetComponent<SpawnPatches>().teleport = true;
                }
            }
            if (currentHitObject.gameObject.tag == "Patches")
            {
                //stop following player
                currentHitObject.gameObject.GetComponent<Patches>().agent.isStopped = true;
            }
            else if (currentHitObject.gameObject.tag != "Patches")
            {
                //restart following player
                GameObject.Find("patchesv2.1(Clone)").GetComponent<Patches>().agent.isStopped = false;
            }
            //Debug.Log(currentHitObject.gameObject.tag);
            //Debug.Log(currentHitObject.transform.tag);
        }
        else
        {
            doorHit = false;
            currentHitDistance = maxDistance;
            currentHitObject = null;
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Debug.DrawLine(origin, origin + direction * currentHitDistance);
        Gizmos.DrawWireSphere(origin + direction * currentHitDistance, sphereRadius);
    }

    IEnumerator RunFromLight()
    {
        coroutineEnd = false;
        yield return new WaitForSeconds(2f);
        //Debug.Log("Elephant Before: " + gameManager.GetComponent<Enemy_Waypoints>().elephantInstance.transform.position);
        gameManager.GetComponent<Enemy_Waypoints>().TeleportElephant();
        //Debug.Log("Elepnat After: " + gameManager.GetComponent<Enemy_Waypoints>().elephantInstance.transform.position);
        gameManager.GetComponent<Enemy_Waypoints>().eleTime = 0;
        //Debug.Log("Eww, Light!");
        coroutineEnd = true;
    }
}
