﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndgameManager : MonoBehaviour {
    public AudioSource patchesAudio;
    // Use this for initialization
    void Start()
    {
        Debug.Log("In start");
        patchesAudio.PlayDelayed(4f);
        StartCoroutine(LoadMenuScene(12f));
        Debug.Log("On it");
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator LoadMenuScene(float delay)
    {
        Debug.Log("In Function");
        yield return new WaitForSeconds(delay);
        Debug.Log("In Function 2");
        SceneManager.LoadScene("Menu");
    }
}
