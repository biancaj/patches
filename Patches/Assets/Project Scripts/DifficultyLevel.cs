﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DifficultyLevel : MonoBehaviour {

    public int difficulty;
    public static bool difficultySet;
    public Text thisText;
    public Text[] notThisText;
	// Use this for initialization
	void Start () {
        difficultySet = false;
    }
	
	// Update is called once per frame
	void Update () {

	}

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("Entered: " + other.transform.tag);
        //if the collider is from the right index finger and a reload of the camera is needed
        if (other.transform.tag == "RightIndex")
        {
            PlayerPrefs.SetInt("Difficulty", difficulty);
            Debug.Log(difficulty);
            Debug.Log(PlayerPrefs.GetInt("Difficulty"));
            difficultySet = true;
            HighlightDifficulty();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(difficultySet&&collision.transform.tag == "RightIndex")
        {
            SceneManager.LoadScene("Main");
        }
    }

    void HighlightDifficulty()
    {
        thisText.color = new Color(255, 0, 0);
        for(int i=0;i<notThisText.Length;i++)
        {
            notThisText[i].color = new Color((50/255), (50 / 255), (50 / 255));
        }
    }
}
