﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SpawnPatches : MonoBehaviour
{

    private GameObject player;  // Player for the NavMesh

    public Clock_Time spawn_time;
    public GameObject patchesPrefab;
    public Transform[] upstairs;
    public Transform[] downstairs;
    public bool teleport;
    public float seconds;
    public bool isPatchesSpawned;
    public bool isTeleported;
    public bool firstSpawn;

    public AudioClip[] patchesSources;
    public AudioSource patchesAudio;

    private GameObject patchesInstance;
    public NavMeshAgent agent;

    // Use this for initialization
   void Start()
    {
        player = GameObject.FindWithTag("PlayerController");
        //patchesAudio = GameObject.FindWithTag("Patches").GetComponent<AudioSource>();

        isPatchesSpawned = false;
        teleport = false;
        isTeleported = false;
        firstSpawn = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (spawn_time.hours == 1 && isPatchesSpawned == false && firstSpawn == false)
        {
            PatchesSpawn();
            firstSpawn = true;
        }
        if (teleport && !isTeleported)
        {
            Invoke("TeleportPatches", seconds);
            isTeleported = true;
        }
        //Debug.Log(player.transform.position);
    }

    //functionality
    AudioClip GetAudioClip(AudioClip[] sources)
    {
        int audioIndex = Random.Range(0, sources.Length);
        AudioClip chosen = sources[audioIndex];
        return chosen;
    }



    void PatchesSpawn()
    {
        Vector3 spawnPosition = new Vector3();
        if (player.transform.position.y >= 34.5)
        {
            spawnPosition = downstairs[0].position;
            //spawnPosition = downstairs[Random.Range(0, downstairs.Length)].position;
        }
        else if (player.transform.position.y < 34.5)
        {
            spawnPosition = upstairs[0].position;
            //spawnPosition = upstairs[Random.Range(0, downstairs.Length)].position;
        }
        patchesInstance = Instantiate(patchesPrefab, spawnPosition, Quaternion.identity);
        agent = patchesInstance.GetComponent<NavMeshAgent>();
        patchesAudio = patchesInstance.GetComponent<AudioSource>();
        isPatchesSpawned = true;
    }

    public void TeleportPatches()
    {
        Vector3 teleportPosition = new Vector3();
        if (player.transform.position.y >= 34.5)
        {
            teleportPosition = downstairs[0].position;
        }
        else if (player.transform.position.y < 34.5)
        {
            teleportPosition = upstairs[0].position;
        }

        //patchesAudio.clip = GetAudioClip(patchesSources);
        //patchesAudio.Play();

        agent.enabled = false;
        patchesInstance.transform.position = teleportPosition;
        agent.enabled = true;
        patchesInstance.GetComponent<Patches>().current_patrol = Random.Range(0, patchesInstance.GetComponent<Patches>().patrolling_points.Length);
        patchesInstance.GetComponent<Patches>().agent.SetDestination(patchesInstance.GetComponent<Patches>().patrolling_points[patchesInstance.GetComponent<Patches>().current_patrol].position);
        //patchesInstance.GetComponent<NavMeshAgent>().isStopped = false;
        patchesInstance.GetComponent<Patches>().triggered = false;
        teleport = false;
        isTeleported = false;
    }
}
