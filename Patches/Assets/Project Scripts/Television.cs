﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class Television : MonoBehaviour {

    public int channel;                 // To hold an int value, determines video channel
    public VideoClip[] video_clips;     // To hold an array of video clips
    public AudioClip[] audio_clips;     // To hold an array of audio clips
    public VideoPlayer videoPlayer;     // To hold the video player component
    public AudioSource audioSource;     // To hold the audio source component
    public bool playmode;               // To hold a boolean value, true for play mode, false for pause mode

 	// Use this for initialization
	void Start () {
        videoPlayer = GetComponent<VideoPlayer>();      // Initialize the component to videoPlayer
        audioSource = GetComponent<AudioSource>();      // Initialize the component to audioPlayer
        playmode = false;                               // Initialzed playmode to false;
        channel = 0;                                    // Start the channel off at 0
	}
	
	// Update is called once per frame
	void Update ()
    {
        CurrentVideo(); // Get current video playing
    }

    /// <summary>
    /// Plays the current video, on the current channel
    /// </summary>
    private void CurrentVideo()
    {
        // if channel == 1, play camera (video and audio)
        if (channel == 1)
        {
            videoPlayer.clip = video_clips[channel - 1];
            audioSource.clip = audio_clips[channel - 1];
            PlayPause();
        }
        // else if channel == 1, play shakelight (video and audio)
        else if (channel == 2)
        {
            videoPlayer.clip = video_clips[channel - 1];
            audioSource.clip = audio_clips[channel - 1];
            PlayPause();

        }
        // else if channel == 1, play watch (video and audio)
        else if (channel == 3)
        {
            videoPlayer.clip = video_clips[channel - 1];
            audioSource.clip = audio_clips[channel - 1];
            PlayPause();
        }
    }
    /// <summary>
    /// Depending on status of playmode, it pauses or plays the current video.
    /// </summary>
    private void PlayPause()
    {
        // if in playmode, then play the current video, else pause the current video (with audio)
        if (playmode)
        {
            videoPlayer.Play();
            audioSource.Play();
        }
        else
        {
            videoPlayer.Pause();
            audioSource.Pause();
        }
    }
}
