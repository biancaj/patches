﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shakelight : MonoBehaviour {

    public OVRInput.Controller rightController;
    public new AudioSource audio;
    public static bool shake;

    //for raycasting: maybe use spherecast to detect stuff in range of spot

    private float waitForDim;//intensity stays at max for 6 secodns then starts lowering intensity
    private Light spot;     //spotlight of the flashlight
    private bool dim;       //bool to state, if light is in dimming mode
    private int shakecounter;

    // Use this for initialization
    void Start () {
        spot = this.GetComponent<Light>();  //get the light component of the spotlight, which the light is supposed to be attached to
        spot.intensity = 0.0f;             //set the spot intensity to 3.27 for the full intensity our light is supposed to have in order to dim it down from full to zero
        waitForDim = 360f;                   //set time to wait until dimming starts to 6 seconds
        audio = GetComponent<AudioSource>();
        shake = false;
        shakecounter = 0;
	}
	
	// Update is called once per frame
	void Update () {
        //Get Oculus input of accelerometer and increase intensity
        if(OVRInput.GetLocalControllerAcceleration(rightController).x>=5f|| OVRInput.GetLocalControllerAcceleration(rightController).y >= 5f||OVRInput.GetLocalControllerAcceleration(rightController).z >= 5f)
        {
            //gradually increase light using lerp
            spot.intensity = Mathf.Lerp(spot.intensity, 1.7f, (1.7f / 100f));
            //if you reached full intensity
            if (spot.intensity >= 1.6f)
            {
                //dimming starts new every time it reaches full intensity
                dim = true;
                // time to wait for dim restets to 6 seconds every time it reaches full intensity
                waitForDim = 360f;
            }
            else
            {
                dim = true;
                waitForDim = 180f;
            }
            shake = true;

            if (!audio.isPlaying && shake)
            {
                audio.Play();
            }
        }
        else
        {
            if (shake)
            {
                shakecounter++;
                if(shakecounter > 30)
                {
                    audio.Stop();
                    shake = false;
                    shakecounter = 0;
                }
            }
        }

        //if the light is in dimming mode
        if (dim)
        {
            ///decrease time to wait until dimming starts
            waitForDim--;
        }

        //if the light is still on, in dimming mode and the time until the dimming starts has passed
        if (dim && waitForDim <= 0.0f)
        {
            //decrease light gradually using Lerp: go from the current intensity to 0, in steps of the full intensity/3600, so that each step will have the same size
            spot.intensity = Mathf.Lerp(spot.intensity, 0.0f, (1.7f / 1200f));
            //if it gets very small and is not caught by Lerp anymore
            if (spot.intensity <= 0.35f)
            {
                //gradually decrease the intensity by 0.001
                spot.intensity -= 0.001f;
            }
            //if the spot intensity is very small and almost not visible
            if (spot.intensity <= 0.1f)
            {
                //set intensity to 0 and light goes out
                spot.intensity = 0f;
            }
        }

        //if the light is out
        if (spot.intensity == 0)
        {
            //get out of dimming mode
            dim = false;
            //reset waiting until the dimming starts
            waitForDim = 360f;
        }
    }
}
