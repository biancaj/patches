﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Patches : MonoBehaviour {

    public Transform[] patrolling_points;
    public bool patches_flashed;
    public NavMeshAgent agent; // NavMesh agent for patches
    public int current_patrol;
    public AudioSource audio_patrol;
    public AudioClip[] audio_clips;

    private GameObject player;  // Player for the NavMesh
    private const int EPSILON = 3;
    public bool triggered;

	// Use this for initialization
	void Start () {
        agent = GetComponent<NavMeshAgent>();
        player = GameObject.FindWithTag("PlayerController");
        patches_flashed = false;
        current_patrol = Random.Range(0, patrolling_points.Length);
        agent.SetDestination(patrolling_points[current_patrol].position);
        triggered = false;
        transform.Rotate(0, -90, 0);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "PlayerController")
        {
            triggered = true;
        }
        if(other.gameObject.layer==14)
        {
            other.GetComponent<Renderer>().material.shader = Shader.Find("Bloody/Doors");
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "PlayerController")
        {
            triggered = false;
        }
        if (other.gameObject.layer == 14)
        {
            other.GetComponent<Renderer>().material.shader = Shader.Find("Standard");
        }
    }

    // Update is called once per frame
    void Update () {
        PatchesNavigation();
    }

    void PatchesNavigation()
    {
        // if statement, if player is inside of bubble then follow player, else continue back to patrolling
        if (triggered)
        {
            // if statement, if patches if flashed my camera then stun him for 5 seconds.
            if (!patches_flashed)
            {
                Vector3 lookRotation = new Vector3(player.transform.position.x, transform.position.y, player.transform.position.z);
                transform.LookAt(lookRotation);
                //transform.Rotate(0, 180, 0);
            }
            agent.SetDestination(player.transform.position);
        }
        else
        {
            //transform.LookAt(new Vector3(patrolling_points[current_patrol].position.x, transform.position.y, patrolling_points[current_patrol].position.z));
            //transform.Rotate(0, 180, 0);
        }

        // if statement, Patches will have a patrolling route as long as no where near the player.
        if ((transform.position - patrolling_points[current_patrol].position).magnitude < EPSILON && !triggered)
        {
            audio_patrol.clip = audio_clips[Random.Range(0, audio_clips.Length)];
            audio_patrol.Play();
            current_patrol = Random.Range(0, patrolling_points.Length);
            agent.SetDestination(patrolling_points[current_patrol].position);
        }
    }

    public void StunPatches()
    {
        StartCoroutine(PatchesStun());
    }

    public IEnumerator PatchesStun()
    {
        agent.isStopped = true;
        yield return new WaitForSeconds(5f);
        agent.isStopped = false;
        patches_flashed = false;
    }

}
