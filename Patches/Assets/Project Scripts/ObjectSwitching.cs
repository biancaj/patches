﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSwitching : MonoBehaviour {

    public GameObject shakelight;
    public GameObject hand;
	// Use this for initialization
	void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {
        //if A button is pressed
        if (OVRInput.GetDown(OVRInput.Button.One))
        {

            if (shakelight.activeSelf == true)
            {
                shakelight.SetActive(false);
                hand.SetActive(true);
            }
            else
            {
                shakelight.SetActive(true);
                hand.SetActive(false);
            }
            //Eric's Trigger idea: use Axis1D.SecondaryIndexTrigger as input
        }
    }
}
