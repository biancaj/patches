﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReloadCamera : MonoBehaviour {

    public Text pictures_prompt;      // To hold the component of the text box
    public CameraScript camScript;  // To hold the script of CameraScript

	// Use this for initialization
	void Start () {
        pictures_prompt = GetComponent<Text>() as Text;   // Initialize the text component into reload_prompt
    }
	
	// Update is called once per frame
	void Update () {
        // if camera needs reloading, displays to reload, otherwise always display pictures remaining
        if (camScript.needsReload)
        {
            pictures_prompt.text = ("Reload Camera");
        }
        else
        {
            pictures_prompt.text = ("Pictures: " + (camScript.pictures.ToString()));
        }
    }
}
