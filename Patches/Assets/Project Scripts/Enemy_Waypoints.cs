﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Waypoints : MonoBehaviour
{

    public Transform[] waypoints;          // To hold the different waypoints
    public GameObject bunnyPrefab;         // To hold the prefab of the bunny
    public GameObject bearOnePrefab;       // To hold the prefab of the first bear
    public GameObject bearTwoPrefab;       // To hold the prefab of the second bear
    public GameObject elephantPrefab;      // To hold the prefab of the elephant
    public GameObject clownPrefab;         // To hold the prefab of the clown
    public GameObject bunnyInstance;       // To hold the instance of the bunny object
    public GameObject bearOneInstance;     // To hold the instance of the bear object
    public GameObject bearTwoInstance;     // To hold the instance of the bear object
    public GameObject elephantInstance;    // To hold the instance of the elephant object
    public GameObject clownInstance;       // To hold the instance of the clown object
    public float bearOne_timer;            // To hold the time in seconds in which the first bear lasts per teleport, manually inserted into unity 
    public float bearTwo_timer;            // To hold the time in seconds in which the second bear lasts per teleport, manually inserted into unity 
    public float bunny_timer;              // To hold the time in seconds in which the bunny lasts per teleport, manually inserted into unity 
    public float elephant_timer;           // To hold the time in seconds in which the elephant lasts per teleport, manually inserted into unity 
    public float clown_timer;              // To hold the time in seconds in which the clown lasts per teleport, manually inserted into unity 
    public float bearOneTime;              // To hold the time for the first bear in delta time, frames
    public float bearTwoTime;              // To hold the time for the second bear in delta time, frames
    public float bunnyTime;                // To hold the time for the bunny in delta time, frames
    public float eleTime;                  // To hold the time for the elephant in delta time, frames
    public float clownTime;                // To hold the time for the clown in delta time, frames
    public bool isBearOneSpawned;          // To hold the boolean value for the first bear, true if uncaptured, and false for camptured
    public bool isBearTwoSpawned;          // To hold the boolean value for the second bear, true if uncaptured, and false for camptured
    public bool isBunnySpawned;            // To hold the boolean value for the bunny, true if uncaptured, and false for camptured
    public bool isElephantSpawned;         // To hold the boolean value for the elephant, true if uncaptured, and false for camptured
    public bool isClownSpawned;            // To hold the boolean value for the elephant, true if uncaptured, and false for camptured
    public bool freeze_bearOne;            // To make the first bear freeze, when light shines on him
    public bool freeze_bearTwo;            // To make the second bear freeze, when light shines on him
    public int bearOneIndex;               // To hold the index of the waypoint for the first bear
    public int bearTwoIndex;               // To hold the index of the waypoint for the second bear
    public int bunnyIndex;                 // To hold the index of the waypoint for the bunny
    public int elephantIndex;              // To hold the index of the waypoint for the elephant
    public int clownIndex;                 // To hold the index of the waypoint for the clown
    public List<int> current_locations = new List<int>(); // To hold a list of integers which will determine the location
    public Transform[] lookAtLocations;
    public Clock_Time ct;
    public GameObject smokeSystem;
    public GameObject confettiSystem;
    public AudioSource clownAudio;
    public AudioClip bearOneAudio;
    public AudioClip bearTwoAudio;
    public AudioClip bunnyAudio;
    public AudioClip elephantAudio;

    //do audioclip or audio source, whatever works best, or different script
    public AudioClip[] elephantSources;
    public AudioClip[] bunnySources;
    public AudioClip[] bearOneSources;
    public AudioClip[] bearTwoSources;
    public AudioClip[] clownSources;

    private Vector3 bearOne_placement;      // To hold the placement location of the first bear
    private Vector3 bearTwo_placement;      // To hold the placement location of the second bear
    private Vector3 bunny_placement;        // To hold the placement location of the bunny
    private Vector3 elephant_placement;     // To hold the placement location of the elephant
    private Vector3 clown_placement;        // To hold the placement location of the clown
    private int bearOne_seconds;            // To hold the conversion from the delta time (frames) and convert them into seconds, for first bear
    private int bearTwo_seconds;            // To hold the conversion from the delta time (frames) and convert them into seconds, for second bear
    private int bunny_seconds;              // To hold the conversion from the delta time (frames) and convert them into seconds, for bunny
    private int elephant_seconds;           // To hold the conversion from the delta time (frames) and convert them into seconds, for elephant
    private int clown_seconds;              // To hold the conversion from the delta time (frames) and convert them into seconds, for clown
    private bool clown_appears;             // To hold the boolean, when clown is first spawned

    // Use this for initialization
    void Start()
    {
        SpawnPlushies();        // Spawns all three plushies
        bearOneTime = 0;        // Initialize bearOneTime to 0 frames
        bearTwoTime = 0;        // Initialize bearTwoTime to 0 frames
        bunnyTime = 0;          // Initialize bearTime to 0 frames
        eleTime = 0;            // Initialize bearTime to 0 frames
        clownTime = 0;          // Initialize bearTime to 0 frames
        bearOne_seconds = 0;    // Initialize bearOne_seconds to 0 seconds
        bearTwo_seconds = 0;    // Initialize bearTwo_seconds to 0 seconds
        bunny_seconds = 0;      // Initialize bunny_seconds to 0 seconds
        elephant_seconds = 0;   // Initialize elephant_seconds to 0 seconds
        clown_seconds = 0;      // Initialize clown_seconds to 0 seconds
        freeze_bearOne = false; // Set freeze_bearOne to false, since he is not caught by light yet
        freeze_bearTwo = false; // Set freeze_bearTwo to false, since he is not caught by light yet
        isClownSpawned = false;
        clown_appears = false;
    }

    // Update is called once per frame
    void Update()
    {
        // spawn the clown at 1am, do this only once
        if (ct.hours == 0 && !isClownSpawned && !clown_appears)
        {
            SpawnClown();
            clown_appears = true;
        }

        // As long as bear isnt frozen, continue the bearOneTime, else stop time
        if (!freeze_bearOne)
        {
            bearOneTime += Time.deltaTime;             // Increment bearOneTime by Time.deltaTime every frame
            bearOne_seconds = (int)(bearOneTime % 60);    // Convert the frames every % 60, to add a second to bearOne_seconds
        }

        // As long as bear isnt frozen, continue the bearbearTwoTime_timer, else stop time
        if (!freeze_bearTwo)
        {
            bearTwoTime += Time.deltaTime;             // Increment bearTwoTime by Time.deltaTime every frame
            bearTwo_seconds = (int)(bearTwoTime % 60);    // Convert the frames every % 60, to add a second to bearTwo_seconds
        }
        bunnyTime += Time.deltaTime;                // Increment bunnyTime by Time.deltaTime every frame
        eleTime += Time.deltaTime;                  // Increment eleTime by Time.deltaTime every frame
        if (isClownSpawned)
        {
            clownTime += Time.deltaTime;                  // Increment clownTime by Time.deltaTime every frame
        }
        bunny_seconds = (int)(bunnyTime % 60);      // Convert the frames every % 60, to add a second to bunny_seconds
        elephant_seconds = (int)(eleTime % 60);     // Convert the frames every % 60, to add a second to elephant_seconds
        clown_seconds = (int)(clownTime % 60);     // Convert the frames every % 60, to add a second to elephant_seconds

        // If bearOneTime reaches 0, and has not been captured yet, then...
        if (bearOne_timer - bearOne_seconds == 0 && isBearOneSpawned && !freeze_bearOne)
        {
            TeleportBearOne(); // Teleport bear to new location
            bearOneTime = 0;   // Re-set bear frame time back to 0
        }

        // If bear_timer reaches 0, and has not been captured yet, then...
        if (bearTwo_timer - bearTwo_seconds == 0 && isBearTwoSpawned && !freeze_bearTwo)
        {
            TeleportBearTwo(); // Teleport bear to new location
            bearTwoTime = 0;   // Re-set bear frame time back to 0
        }

        // If bunny_timer reaches 0, and has not been captured yet, then...
        if (bunny_timer - bunny_seconds == 0 && isBunnySpawned)
        {
            TeleportBunny();    // Teleport bunny to new location
            bunnyTime = 0;      // Re-set bunny frame time back to 0
        }

        // If elephant_timer reaches 0, and has not been captured yet, then...
        if (elephant_timer - elephant_seconds == 0 && isElephantSpawned)
        {
            TeleportElephant(); // Teleport elephant to new location
            eleTime = 0;        // Re-set elephant frame time back to 0
        }
        // If clown_timer reaches 0, and has not been captured yet, then...
        if (clown_timer - clown_seconds == 0 && isClownSpawned)
        {
            TeleportClown(); // Teleport elephant to new location
            clownTime = 0;        // Re-set elephant frame time back to 0
        }
    }


    //**************************************************************Mechanics*********************************************************************************************

    /// <summary>
    /// This function get's an index that isnt being used by other enemies.
    /// It stores an integer into a List<int> current_locations.
    /// It makes sure to return a value that isnt within the List.
    /// </summary>
    /// <returns>
    /// An integer, from 0 - waypoints.length (0-14)
    /// </returns>
    private int GetIndex()
    {
        int index = Random.Range(0, waypoints.Length);  // Declare and initialize index to be a random number from 0-14 (waypoint.length)

        // while loop: Validation method that if the integer "index" is already in the List "current_locations" then,
        // try for a new random integer, until one is not located in that list.
        while (current_locations.Contains(index))
        {
            index = Random.Range(0, waypoints.Length);
        }

        current_locations.Add(index);   // Add integer to List

        return index;                   // Return integer
    }

    AudioClip GetAudioClip(AudioClip[] sources)
    {
        int audioIndex = Random.Range(0, sources.Length);
        AudioClip chosen = sources[audioIndex];
        return chosen;
    }

    public void RandomVoiceTime(float starttime, float endtime, AudioSource voiceline)
    {
        float timeToStart = Random.Range(starttime + 1, endtime - 1);
        voiceline.PlayDelayed(timeToStart);
    }

    public void FindAndLookAt(int index, GameObject instance)
    {
        Transform toLookAt = lookAtLocations[index];
        Vector3 lookPosition = new Vector3(lookAtLocations[index].position.x, instance.transform.position.y, lookAtLocations[index].position.z);
        instance.transform.LookAt(lookPosition);
    }

    IEnumerator ClipToSource(AudioSource mySource, AudioClip myClip, float timer, AudioClip voiceLine)
    {
        mySource.clip = myClip;
        mySource.Play();
        yield return new WaitForSeconds(mySource.clip.length);
        mySource.clip = voiceLine;
        RandomVoiceTime(0, timer - mySource.clip.length, mySource);
    }

    //**************************************************************Teleporting*********************************************************************************************


    /// <summary>
    /// This function teleports the bear to a new location, as long as it hasnt been captured
    /// </summary>
    public void TeleportBearOne()
    {
        // if bearOne is still spawned in the game, then it hasnt been captured
        if (isBearOneSpawned)
        {
            GameObject pInstance = Instantiate(smokeSystem, bearOneInstance.transform.position, Quaternion.identity); //bearOneInstance.transform.rotation);
            Destroy(pInstance, 1f);
            int previousIndex = bearOneIndex;                      // Give bearOne index to a temporary variable, use temp variable as reference to remove from list
            bearOneIndex = GetIndex();                             // Get new index for the new teleporting path for the bearOne
            current_locations.Remove(previousIndex);            // Remove the integer from previous location out of the List "current_locations," this prevents using the same location
            bearOne_placement = new Vector3(waypoints[bearOneIndex].position.x, waypoints[bearOneIndex].position.y - 0.601f, waypoints[bearOneIndex].position.z);     // Set the placement for the new location
            bearOneInstance.transform.position = bearOne_placement;   // Move the bearOne instance to the new location
            FindAndLookAt(bearOneIndex, bearOneInstance);

            AudioClip voiceLine = GetAudioClip(bearOneSources);
            AudioSource mySource = bearOneInstance.GetComponent<AudioSource>();

            StartCoroutine(ClipToSource(mySource, bearOneAudio, bearOne_timer, voiceLine));

            //mySource.clip = voiceLine;
            //RandomVoiceTime(0, bearOne_timer, mySource);
        }
    }


    /// <summary>
    /// This function teleports the bear to a new location, as long as it hasnt been captured
    /// </summary>
    public void TeleportBearTwo()
    {
        // if bearTwo is still spawned in the game, then it hasnt been captured
        if (isBearTwoSpawned)
        {
            GameObject pInstance = Instantiate(smokeSystem, bearOneInstance.transform.position, Quaternion.identity); //bearInstance.transform.rotation);
            Destroy(pInstance, 1f);
            int previousIndex = bearTwoIndex;                      // Give bearTwo index to a temporary variable, use temp variable as reference to remove from list
            bearTwoIndex = GetIndex();                             // Get new index for the new teleporting path for the bearTwo
            current_locations.Remove(previousIndex);            // Remove the integer from previous location out of the List "current_locations," this prevents using the same location
            bearTwo_placement = new Vector3(waypoints[bearTwoIndex].position.x, waypoints[bearTwoIndex].position.y - 0.601f, waypoints[bearTwoIndex].position.z);    // Set the placement for the new location
            bearTwoInstance.transform.position = bearTwo_placement;   // Move the bearTwo instance to the new location
            FindAndLookAt(bearTwoIndex, bearTwoInstance);
            //bearTwoInstance.transform.Rotate(0, 270, 0);

            AudioClip voiceLine = GetAudioClip(bearTwoSources);
            AudioSource mySource = bearTwoInstance.GetComponent<AudioSource>();

            StartCoroutine(ClipToSource(mySource, bearTwoAudio, bearTwo_timer, voiceLine));

            //mySource.clip = voiceLine;
            //RandomVoiceTime(0, bearTwo_timer, mySource);
        }
    }

    /// <summary>
    /// This function teleports the bunny to a new location, as long as it hasnt been captured
    /// </summary>
    public void TeleportBunny()
    {
        // if bunny is still spawned in the game, then it hasnt been captured
        if (isBunnySpawned)
        {
            GameObject pInstance = Instantiate(smokeSystem, bunnyInstance.transform.position, Quaternion.identity); //bunnyInstance.transform.rotation);
            Destroy(pInstance, 1f);
            int previousIndex = bunnyIndex;                          // Give bunny index to a temporary variable, use temp variable as reference to remove from list
            bunnyIndex = GetIndex();                                 // Get new index for the new teleporting path for the bunny
            current_locations.Remove(previousIndex);                 // Remove the integer from previous location out of the List "current_locations," this prevents using the same location
            bunny_placement = waypoints[bunnyIndex].position;        // Set the placement for the new location
            bunnyInstance.transform.position = bunny_placement;      // Move the bunny instance to the new location
            FindAndLookAt(bunnyIndex, bunnyInstance);

            AudioClip voiceLine = GetAudioClip(bunnySources);
            AudioSource mySource = bunnyInstance.GetComponent<AudioSource>();

            StartCoroutine(ClipToSource(mySource, bunnyAudio, bunny_timer, voiceLine));

            //mySource.clip = voiceLine;
            //RandomVoiceTime(0, bunny_timer, mySource);
        }
    }

    public void TeleportClown()
    {
        // if bunny is still spawned in the game, then it hasnt been captured
        if (isClownSpawned)
        {
            GameObject cpInstance = Instantiate(confettiSystem, clownInstance.transform.position, Quaternion.identity); //clownInstance.transform.rotation);
            Destroy(cpInstance, 1f);
            int previousIndex = clownIndex;                          // Give clown index to a temporary variable, use temp variable as reference to remove from list
            clownIndex = GetIndex();                                 // Get new index for the new teleporting path for the clown
            current_locations.Remove(previousIndex);                 // Remove the integer from previous location out of the List "current_locations," this prevents using the same location
            clown_placement = new Vector3(waypoints[clownIndex].position.x, waypoints[clownIndex].position.y - 0.722f, waypoints[clownIndex].position.z);
            clownInstance.transform.position = clown_placement;      // Move the clown instance to the new location
            FindAndLookAt(clownIndex, clownInstance);

            clownAudio.Play();
            AudioClip voiceLine = GetAudioClip(clownSources);
            AudioSource mySource = clownInstance.GetComponent<AudioSource>();
            mySource.clip = voiceLine;
            RandomVoiceTime(0, clown_timer, mySource);

            //Debug.Log("Clown Re-Positioned at location: " + clownIndex);
            ////clownInstance.transform.Rotate(new Vector3(0, 180, 0));
        }
    }

    /// <summary>
    /// This function teleports the elephant to a new location, as long as it hasnt been captured
    /// </summary>
    public void TeleportElephant()
    {
        // if elephant is still spawned in the game, then it hasnt been captured
        if (isElephantSpawned)
        {
            GameObject pInstance = Instantiate(smokeSystem, elephantInstance.transform.position, Quaternion.identity);//elephantInstance.transform.rotation);
            Destroy(pInstance, 1f);
            int previousIndex = elephantIndex;                          // Give elephant index to a temporary variable, use temp variable as reference to remove from list
            elephantIndex = GetIndex();                                 // Get new index for the new teleporting path for the elephant
            current_locations.Remove(previousIndex);                    // Remove the integer from previous location out of the List "current_locations," this prevents using the same location
            elephant_placement = new Vector3(waypoints[elephantIndex].position.x, waypoints[elephantIndex].position.y + 0.108f, waypoints[elephantIndex].position.z);      // Set the placement for the new location
            elephantInstance.transform.position = elephant_placement;   // Move the Elephant instance to the new location
            FindAndLookAt(elephantIndex, elephantInstance);
            //elephantInstance.transform.Rotate(0, 270, 0);

            AudioClip voiceLine = GetAudioClip(elephantSources);
            AudioSource mySource = elephantInstance.GetComponent<AudioSource>();

            StartCoroutine(ClipToSource(mySource, elephantAudio, elephant_timer, voiceLine));
            //mySource.clip = voiceLine;
            //RandomVoiceTime(0, elephant_timer, mySource);
        }
    }



    //**************************************************************Spawning*********************************************************************************************



    /// <summary>
    /// This function will spawn all of the plushies at the start of the game
    /// </summary>
    void SpawnPlushies()
    {
        // ----------------------------------------------------------------------
        // *************************** Bear One Spawn ***************************
        // ----------------------------------------------------------------------
        isBearOneSpawned = true;                                                                   // Makes the boolean value of isBearOneSpawned == true, this means that the bearOne hasn't been captured
        bearOneIndex = GetIndex();                                                                 // Get the index for the teleporting path for the bearOne
        bearOne_placement = new Vector3(waypoints[bearOneIndex].position.x, waypoints[bearOneIndex].position.y - 0.601f, waypoints[bearOneIndex].position.z);                                    // Set the placement for the location is which to spawn
        bearOneInstance = Instantiate(bearOnePrefab, bearOne_placement, Quaternion.identity);      // Instantiate an instance of the bearOne and set it's location to the placement
        FindAndLookAt(bearOneIndex, bearOneInstance);

        AudioClip voiceLine = GetAudioClip(bearOneSources);
        AudioSource mySource = bearOneInstance.GetComponent<AudioSource>();

        StartCoroutine(ClipToSource(mySource, bearOneAudio, bearOne_timer, voiceLine));

        //mySource.clip = voiceLine;
        //RandomVoiceTime(0, bearOne_timer, mySource);


        // ----------------------------------------------------------------------
        // *************************** Bear Two Spawn ***************************
        // ----------------------------------------------------------------------
        isBearTwoSpawned = true;                                                                   // Makes the boolean value of isBearTwoSpawned == true, this means that the bearTwo hasn't been captured
        bearTwoIndex = GetIndex();                                                                 // Get the index for the teleporting path for the bearTwo
        bearTwo_placement = new Vector3(waypoints[bearTwoIndex].position.x, waypoints[bearTwoIndex].position.y - 0.601f, waypoints[bearTwoIndex].position.z);                                     // Set the placement for the location is which to spawn
        bearTwoInstance = Instantiate(bearTwoPrefab, bearTwo_placement, Quaternion.identity);      // Instantiate an instance of the bearTwo and set it's location to the placement
        FindAndLookAt(bearTwoIndex, bearTwoInstance);
        //bearTwoInstance.transform.Rotate(0, 270, 0);

        voiceLine = GetAudioClip(bearTwoSources);
        mySource = bearTwoInstance.GetComponent<AudioSource>();

        StartCoroutine(ClipToSource(mySource, bearOneAudio, bearTwo_timer, voiceLine));

        //mySource.clip = voiceLine;
        //RandomVoiceTime(0, bearTwo_timer, mySource);
        //Debug.Log("Bear Two Positioned at location: " + bearTwoIndex);


        // ----------------------------------------------------------------------
        // ***************************** Bunny Spawn ****************************
        // ----------------------------------------------------------------------
        isBunnySpawned = true;                                                                      // Makes the boolean value of isBunnySpawned == true, this means that the bunny hasn't been captured
        bunnyIndex = GetIndex();                                                                    // Get the index for the teleporting path for the bunny
        bunny_placement = waypoints[bunnyIndex].position;                                           // Set the placement for the location is which to spawn
        bunnyInstance = Instantiate(bunnyPrefab, bunny_placement, Quaternion.identity);             // Instantiate an instance of the bunny and set it's location to the placement
        FindAndLookAt(bunnyIndex, bunnyInstance);

        voiceLine = GetAudioClip(bunnySources);
        mySource = bunnyInstance.GetComponent<AudioSource>();

        StartCoroutine(ClipToSource(mySource, bunnyAudio, bunny_timer, voiceLine));

        //mySource.clip = voiceLine;
        //RandomVoiceTime(0, bunny_timer, mySource);
        //Debug.Log("Bunny Positioned at location: " + bunnyIndex);


        // ----------------------------------------------------------------------
        // *************************** Elephant Spawn ***************************
        // ----------------------------------------------------------------------
        isElephantSpawned = true;                                                                   // Makes the boolean value of isElephantSpawned == true, this means that the elephant hasn't been captured
        elephantIndex = GetIndex();                                                                 // Get the index for the teleporting path for the elephant
        elephant_placement = new Vector3(waypoints[elephantIndex].position.x, waypoints[elephantIndex].position.y + 0.108f, waypoints[elephantIndex].position.z);                                     // Set the placement for the location is which to spawn
        elephantInstance = Instantiate(elephantPrefab, elephant_placement, Quaternion.identity);    // Instantiate an instance of the elephant and set it's location to the placement
        FindAndLookAt(elephantIndex, elephantInstance);
        //elephantInstance.transform.Rotate(0, 270, 0);

        voiceLine = GetAudioClip(elephantSources);
        mySource = elephantInstance.GetComponent<AudioSource>();

        StartCoroutine(ClipToSource(mySource, elephantAudio, elephant_timer, voiceLine));

        //mySource.clip = voiceLine;
        //RandomVoiceTime(0, elephant_timer, mySource);
        //Debug.Log("Elephant Positioned at location: " + elephantIndex);
    }

    void SpawnClown()
    {
        isClownSpawned = true;                                                                       // Makes the boolean value of isclownSpawned == true, this means that the clown hasn't been captured
        clownIndex = GetIndex();                                                                     // Get the index for the teleporting path for the clown
        clown_placement = new Vector3(waypoints[clownIndex].position.x, waypoints[clownIndex].position.y - 0.722f, waypoints[clownIndex].position.z);                                             // Set the placement for the location is which to spawn
        clownInstance = Instantiate(clownPrefab, clown_placement, waypoints[clownIndex].rotation);                // Instantiate an instance of the clown and set it's location to the placement
        FindAndLookAt(clownIndex, clownInstance);
        clownAudio = GameObject.FindWithTag("Horn").GetComponent<AudioSource>();
        clownAudio.Play();
        //Debug.Log("clown Positioned at location: " + clownIndex);
        //clownInstance.transform.Rotate(new Vector3(0, 180, 0));
        AudioClip voiceLine = GetAudioClip(clownSources);
        AudioSource mySource = clownInstance.GetComponent<AudioSource>();
        mySource.clip = voiceLine;
        RandomVoiceTime(0, clown_timer, mySource);
    }

}

