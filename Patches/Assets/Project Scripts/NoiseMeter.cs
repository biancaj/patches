﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NoiseMeter : MonoBehaviour {

    //public Text noise;                      // To hold the component of the text box
    public GameObject shakelight;           // To hold the game object of the shakelight 
    public GameObject cameraFlash;          // To hold the game object of the camera flash
    public int noiseRange;                 // To hold the range of noise from 1-100
    private OVRPlayerController opc;
    public int difficulty;                  //To get the game's difficulty
    public Light[] lights;
    public GameObject clown;

    private Shakelight shakelightScript;    // To hold the shakelight script
    private CameraScript cameraScript;      // To hold the shakelight script
    private int decreaseOffset;             // To hold the timeCounter in which to slow down the decrease of noise meter
    private int timeCounter;                // To count the time passed in frames
    private Enemy_Waypoints enemywaypoints;
    

    // Use this for initialization
    void Start () {
        //noise = GetComponent<Text>() as Text;                       // Initialize the text component into noise
        shakelightScript = shakelight.GetComponent<Shakelight>();   // Initialize the script within the shakelight component
        cameraScript = cameraFlash.GetComponent<CameraScript>();    // Initialize the script within the shakelight component
        opc = GameObject.FindWithTag("PlayerController").GetComponent<OVRPlayerController>();
        decreaseOffset = 0;                                         // Initialize decrease offset to 0
        timeCounter = 0;                                            // Initialize time counter to 0
        difficulty = PlayerPrefs.GetInt("Difficulty", 1);
        enemywaypoints = GameObject.Find("GameManager").GetComponent<Enemy_Waypoints>();
    }
	
	// Update is called once per frame
	void Update () {
        timeCounter++;
        // if shakelight audio is playing, increase noiseRange, as long as it doesn't go past 100, go to this statement every half a second
        if (shakelightScript.audio.isPlaying && noiseRange < 100 && timeCounter % 80 == 0)
        {
            //increase noise range by 1
            noiseRange += 3*difficulty;
        }
        // if shutter audio is playing, increase noiseRange, as long as it doesn't go past 100, go to this statement every quarter second
        if (cameraScript.camShutter.isPlaying && noiseRange < 100 & timeCounter % 80 == 0)
        {
            //increase noise range by 1
            noiseRange += 3 * difficulty;
        }
        // if reload audio is playing, increase noiseRange, as long as it doesn't go past 100, go to this statement every quarter second
        if (cameraScript.camReload.isPlaying && noiseRange < 100 & timeCounter % 80 == 0)
        {
            //increase noise range by 1
            noiseRange += 1 * difficulty;
        }
        // if running audio is playing, increase noiseRange, as long as it doesn't go past 100, go to this statement every half a second
        if (opc.running.isPlaying && noiseRange < 100 && timeCounter % 80 == 0)
        {
            //increase noise range by 1
            noiseRange += 4 * difficulty;
        }
        // if clown horn is playing, increase noiseRange, as long as it doesn't go past 100
        if (enemywaypoints.clownAudio && enemywaypoints.clownAudio.isPlaying && noiseRange < 100 && timeCounter % 120 == 0)
        {
            //increase noise range by 1
            noiseRange += 1 * difficulty;
        }


        if (enemywaypoints.clownInstance && enemywaypoints.clownInstance.GetComponent<AudioSource>().isPlaying && noiseRange < 100 && timeCounter % 360 == 0)
        {
            //increase noise range by 1
            noiseRange += 1 * difficulty;
        }

        // if audio is not playing, decrease noiseRange, as long as it does not go under 0
        if (!shakelightScript.audio.isPlaying && noiseRange > 0 && !cameraScript.camShutter.isPlaying && noiseRange > 0 && !cameraScript.camReload.isPlaying && noiseRange > 0)
        {
            decreaseOffset++;
            // when decreasing, wait 3 seconds before start to decrease.
            if(decreaseOffset > 180)
            {
                //decrease noise range by 5
                noiseRange -= 7;
                //set the decreasing offset to 0, so it is ready to repeat the procedure, if necessary
                decreaseOffset = 0;
            }
        }

        //constraints: noise range remains between 0 and 100
        noiseRange = (noiseRange < 0) ? 0 : noiseRange;
        noiseRange = (noiseRange > 100) ? 100 : noiseRange;
        //display noise meter on watch
        //noise.text = ((noiseRange.ToString()));
        //Debug.Log(noiseRange);
        NoiseMeterLighting();
    }

    //void GetNoiseRange()
    //{

    //}

    void NoiseMeterLighting()
    {
        for(int i=0;i<lights.Length;i++)
        {
            if(noiseRange>=((100/12)*(i+1)))
            {
                lights[i].enabled = true;
            }
            else
            {
                lights[i].enabled = false;
            }
        }
    }
}
