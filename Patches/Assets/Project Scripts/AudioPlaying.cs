﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPlaying : MonoBehaviour {

    public new AudioSource audio;

    void Start()
    {
        audio = GetComponent<AudioSource>();
    }
    private void Update()
    {
        if (!audio.isPlaying)
        {
            audio.Play();
        }
    }
}
