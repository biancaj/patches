﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HearingPlayer : MonoBehaviour
{

    private GameObject flashlight;          // To hold the spotlight object, has shakelight script
    private GameObject camera;              // To hold the KodakishCamera, has cameraScript
    private Shakelight shakelightScript;    // To hold the Shakelight script
    private CameraScript cameraScript;      // To hold the CameraScript script

    public GameObject gameManager;
    public bool continuePlaying;
    public bool coroutineEnd;

    private void Start()
    {
        camera = GameObject.Find("KodakishCamera");                 // Intialize the KodakishCamera game object to camera
        flashlight = GameObject.Find("Spotlight");                  // Initialize the Spotlight game object to flashlight
        shakelightScript = flashlight.GetComponent<Shakelight>();   // Initiailze the Shakelight script to shakelightScript
        cameraScript = camera.GetComponent<CameraScript>();         // Initialize the CameraScript script to cameraScript
        gameManager = GameObject.Find("GameManager");
        continuePlaying = false;
        coroutineEnd = true;
    }

    private void OnTriggerStay(Collider other)
    {
        // If player is in the trigger collider
        if (other.gameObject.tag == "PlayerController")
        {
            // If the player makes any noise within the collider, do something
            if (shakelightScript.audio.isPlaying || cameraScript.camShutter.isPlaying|| cameraScript.camReload.isPlaying)
            {
                if (gameManager.GetComponent<Enemy_Waypoints>() && coroutineEnd == true)
                {
                    StartCoroutine(RunFromNoise());
                    ////Debug.Log("Bunny Before: " + gameManager.GetComponent<Enemy_Waypoints>().bunnyInstance.transform.position);
                    //gameManager.GetComponent<Enemy_Waypoints>().TeleportBunny();
                    ////Debug.Log("Bunny After: " + gameManager.GetComponent<Enemy_Waypoints>().bunnyInstance.transform.position);
                    //gameManager.GetComponent<Enemy_Waypoints>().bunny_timer = 750;
                    ////Debug.Log("What's that sound?!?!");
                }
            }
        }
    }

    private void Update()
    {
        if (continuePlaying == true)
        {
            //Debug.Log("Bunny Before: " + gameManager.GetComponent<Enemy_Waypoints>().bunnyInstance.transform.position);
            gameManager.GetComponent<Enemy_Waypoints>().TeleportBunny();
            //Debug.Log("Bunny After: " + gameManager.GetComponent<Enemy_Waypoints>().bunnyInstance.transform.position);
            gameManager.GetComponent<Enemy_Waypoints>().bunnyTime = 0;
            //gameManager.GetComponent<Enemy_Waypoints>().bunny_timer = gameManager.GetComponent<Enemy_Waypoints>().bunnyInterval;
            //Debug.Log("What's that sound?!?!");
            continuePlaying = false;
        }
    }

    IEnumerator RunFromNoise()
    {
        continuePlaying = false;
        coroutineEnd = false;
        yield return new WaitForSeconds(2f);
        continuePlaying = true;
        coroutineEnd = true;
        //Debug.Log(continuePlaying);
    }
}