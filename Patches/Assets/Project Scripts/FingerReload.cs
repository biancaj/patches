﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FingerReload : MonoBehaviour {

    public CameraScript camScript;  //The CameraScript component of the camera, in order to modify, whether the camera needs reload or not

    //when the trigger collider of the camera button is hit by a collider
    void OnTriggerEnter(Collider other)
    {
        //if the collider is from the right index finger and a reload of the camera is needed
        if(other.transform.tag == "RightIndex" && camScript.needsReload == true)
        {
            //play reloading sound
            camScript.camReload.Play();
            //reload is not needed any more
            camScript.needsReload = false;
        }
    }
}
