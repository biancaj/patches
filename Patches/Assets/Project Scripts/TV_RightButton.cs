﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TV_RightButton : MonoBehaviour {

    private bool clickedRight;      // To hold a boolean value, true when enter, false when exit

    private Television television;  // To hold the Television script, used to get the current channel playing

    // Use this for initialization
    void Start()
    {
        television = GameObject.Find("polySurface15").GetComponent<Television>();   // Initialize the script into television
        clickedRight = false;                                                       // start off as right button hasn't been touched
    }

    private void OnCollisionEnter(Collision collision)
    {
        // if collider collides with right index finger, and also when it hasnt been clicked before
        if (collision.transform.tag == "RightIndex" && !clickedRight)// && !television.videoPlayer.isPlaying)
        {
            //if television current channel is less than 3, then add to current channel by 1, then set clickedRight to true, so it wont keep adding 
            if (television.channel < 3 && television.channel > 0)
            {
                television.channel++;
                //Debug.Log("Current Channel: " + television.channel);
                clickedRight = true;
                //Debug.Log(clickedRight);
            }
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        // if collider from finger leaves the button collider, then set clickedRight to false.
        if (collision.transform.tag == "RightIndex" && clickedRight)
        {
            clickedRight = false;
            //Debug.Log(clickedRight);
        }
    }
}
