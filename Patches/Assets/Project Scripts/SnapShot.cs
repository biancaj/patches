﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnapShot : MonoBehaviour {

    public Light flash;                     // To hold the flash of the camera
    public Enemy_Waypoints enemy_waypoints; // To hold the enemy_waypoints scripts
    public AudioSource bearOne_captured;       // To hold an audio source: When the bear is captured
    public AudioSource bearTwo_captured;       // To hold an audio source: When the bear is captured
    public AudioSource bunny_captured;      // To hold an audio source: When the bunny is captured
    public AudioSource elephant_captured;   // To hold an audio source: When the elephant is captured
    public AudioSource patches_captured;   // To hold an audio source: When Patches is captured 
    public AudioSource clown_captured;   // To hold an audio source: When Clown is captured 
    public GameObject currentHitObject; // To hold the current object being hit.
    public float sphereRadius;          // To set the radius of the sphere
    public float maxDistance;           // To hold the max distance of sphere
    public LayerMask layerMask;         // To hold a set of layer masks, to decide what the sphere cast can collide to or not

    public GameObject smokeSystem;


    private Vector3 origin;             // To origin where teh sphere should start
    private Vector3 direction;          // To hold direction in which ray is gonna be shot in 
    private float currentHitDistance;   // Distance to current object
    private GameObject gameManager;

    public AudioSource parentsHitSound;
    public GameOver parentsHitCounter;
    public bool doorHit;

    // Use this for initialization
    void Start () {
        gameManager = GameObject.FindGameObjectWithTag("Game Manager");
        enemy_waypoints = GameObject.Find("GameManager").GetComponent<Enemy_Waypoints>();   // Initialize the Enemy_Waypoint script into enemy_waypoints
        parentsHitCounter = GameObject.FindGameObjectWithTag("Game Manager").GetComponent<GameOver>();
        doorHit = false;
    }

    /// <summary>
    /// This function is to be used for when the camera takes a picture of the plushies
    /// Used to capture, and destroy the plushies from within the game
    /// </summary>
    /// <param name="other"></param>
   

    private void Update()
    {
        origin = transform.position;    // Origin initialized to 
        direction = transform.forward;
        RaycastHit hit;
        if (Physics.SphereCast(origin, sphereRadius, direction, out hit, maxDistance, layerMask, QueryTriggerInteraction.UseGlobal)&&flash.enabled==true)
        {
            currentHitObject = hit.transform.gameObject;
            currentHitDistance = hit.distance;
            if(flash.enabled == true)
            {
                if (currentHitObject.gameObject.tag == "ShyLight")
                {
                    GameObject pInstance = Instantiate(smokeSystem, currentHitObject.transform.position, Quaternion.identity);
                    Destroy(pInstance, 1f);
                    enemy_waypoints.current_locations.Remove(enemy_waypoints.elephantIndex);    // Remove the integer int he current_location List
                    elephant_captured.Play();                                                   // Play sound when elephant is captured                
                    enemy_waypoints.isElephantSpawned = false;                                  // Set isElephantSpawned to false, making it no longer able to teleport
                    Destroy(currentHitObject.gameObject);                                                  // Destroy the elephant game object
                }
                if (currentHitObject.gameObject.tag == "ShyNoise")
                {
                    GameObject pInstance = Instantiate(smokeSystem, currentHitObject.transform.position, Quaternion.identity);
                    Destroy(pInstance, 1f);
                    enemy_waypoints.current_locations.Remove(enemy_waypoints.bunnyIndex);       // Remove the integer int he current_location List
                    bunny_captured.Play();                                                      // Play sound when bunny is captured
                    enemy_waypoints.isBunnySpawned = false;                                     // Set isBunnySpawned to false, making it no longer able to teleport
                    Destroy(currentHitObject.gameObject);                                                  // Destroy the bunny game object
                }
                if (currentHitObject.gameObject.name == "Love Bear 1(Clone)")
                {
                    GameObject pInstance = Instantiate(smokeSystem, currentHitObject.transform.position, Quaternion.identity);
                    Destroy(pInstance, 1f);
                    enemy_waypoints.current_locations.Remove(enemy_waypoints.bearOneIndex);        // Remove the integer int he current_location List
                    bearOne_captured.Play();                                                       // Play sound when bear is captured                
                    enemy_waypoints.isBearOneSpawned = false;                                      // Set isBearSpawned to false, making it no longer able to teleport
                    Destroy(currentHitObject.gameObject);                                                  // Destroy the bear game object
                }
                if (currentHitObject.gameObject.name == "Love Bear 2(Clone)")
                {
                    GameObject pInstance = Instantiate(smokeSystem, currentHitObject.transform.position, Quaternion.identity);
                    Destroy(pInstance, 1f);
                    enemy_waypoints.current_locations.Remove(enemy_waypoints.bearTwoIndex);        // Remove the integer int he current_location List
                    bearTwo_captured.Play();                                                       // Play sound when bear is captured                
                    enemy_waypoints.isBearTwoSpawned = false;                                      // Set isBearSpawned to false, making it no longer able to teleport
                    Destroy(currentHitObject.gameObject);                                                  // Destroy the bear game object
                }
                if (currentHitObject.gameObject.tag == "Clown")
                {
                    GameObject pInstance = Instantiate(smokeSystem, currentHitObject.transform.position, Quaternion.identity);
                    Destroy(pInstance, 1f);
                    enemy_waypoints.current_locations.Remove(enemy_waypoints.clownIndex);        // Remove the integer int he current_location List
                    clown_captured.Play();                                                      // Play sound when clown is captured                
                    enemy_waypoints.isClownSpawned = false;                                      // Set isBearSpawned to false, making it no longer able to teleport
                    Destroy(currentHitObject.gameObject);                                        // Destroy the clown game object
                }
                if (currentHitObject.gameObject.tag == "Patches" && enemy_waypoints.isBearOneSpawned==false && enemy_waypoints.isBearTwoSpawned == false && enemy_waypoints.isBunnySpawned == false && enemy_waypoints.isElephantSpawned == false)
                {
                    GameObject pInstance = Instantiate(smokeSystem, currentHitObject.transform.position, Quaternion.identity);
                    Destroy(pInstance, 1f);
                    Debug.Log("Got Patches!");
                    //sound?
                    patches_captured.Play();
                    //set PatchesSpawned to false
                    gameManager.GetComponent<SpawnPatches>().isPatchesSpawned = false;
                    //do gameOver for isPatches spawned==false
                    //destroy Patches
                    Destroy(currentHitObject.gameObject);
                }
                else if(currentHitObject.gameObject.tag=="Patches")
                {
                    GameObject.FindWithTag("Patches").GetComponent<Patches>().StunPatches();
                    GameObject.FindWithTag("Patches").GetComponent<Patches>().patches_flashed = true;
                }
                if (currentHitObject.gameObject.tag == "Parents Door" && doorHit == false)
                {

                    if (parentsHitCounter.wakeUpCounter == 1 && doorHit==false)
                    {
                        parentsHitSound.clip = gameManager.GetComponent<GameOver>().GetAudioClip(gameManager.GetComponent<GameOver>().parentsAudioEnd);
                        parentsHitSound.Play();
                        gameManager.GetComponent<GameOver>().wakeUpParents = true;
                    }
                    else if(doorHit==false)
                    {
                        //parentsHitCounter++;
                        parentsHitSound.clip = gameManager.GetComponent<GameOver>().GetAudioClip(gameManager.GetComponent<GameOver>().parentsAudioWarning);
                        parentsHitSound.Play();
                    }
                    doorHit = true;
                }
                if (currentHitObject.gameObject.tag != "Parents Door" && doorHit == true)
                {
                    parentsHitCounter.wakeUpCounter++;
                    doorHit = false;
                }
            }

            else
            {
                doorHit = false;
            }
            
        }
        else
        {
            currentHitDistance = maxDistance;
            currentHitObject = null;
        }
    }

    private void OnDrawGizmosSelected()
    {
        if (flash.enabled == true)
        {
            Gizmos.color = Color.red;
            Debug.DrawLine(origin, origin + direction * currentHitDistance);
            Gizmos.DrawWireSphere(origin + direction * currentHitDistance, sphereRadius);
        }
    }
}
