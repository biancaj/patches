﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour {

    public Enemy_Waypoints getCharacterStatus;  // To get the status of each enemy, if all enemies are caught
    public SpawnPatches getPatchesStatus;
    public Camera overlayCam;                   // To hold the overlay camera
    public GameObject wintext;                  // To hold the 3D UI, for Win
    public GameObject lostText;                 // To hold the 3D UI, for lost
    public NoiseMeter noise;                    // To get the noise meter ammount, over 100 is game over
    public Clock_Time currentTime;              // To get the current game play time, over 6 hours is game over
    public CameraScript pics;                   // To get the numbers of pictures remaining, if 0, game over
    public bool wakeUpParents;
    public int wakeUpCounter;
    public Light houseLights;

    public AudioClip[] parentsAudioWarning;
    public AudioClip[] parentsAudioEnd;
    public AudioSource noiseEnd;
    bool audioSelected;

    // Use this for initialization
    void Start () {
        getCharacterStatus = GetComponent<Enemy_Waypoints>();   // Get component in script
        getPatchesStatus = GetComponent<SpawnPatches>();   // Get component in script
        wakeUpParents = false;
        wakeUpCounter = 0;
        audioSelected = false;
    }

    //functionality
    public AudioClip GetAudioClip(AudioClip[] sources)
    {
        int audioIndex = Random.Range(0, sources.Length);
        AudioClip chosen = sources[audioIndex];
        return chosen;
    }

    // Update is called once per frame
    void Update () {
        // If all plushies camptured, you win the game
        if (getCharacterStatus.isBearOneSpawned == false && getCharacterStatus.isBearTwoSpawned == false && getCharacterStatus.isBunnySpawned == false && getCharacterStatus.isElephantSpawned == false && getPatchesStatus.isPatchesSpawned == false && getCharacterStatus.isClownSpawned == false && getPatchesStatus.firstSpawn == true)
        {
            //Time.timeScale = 0;         // Stop time in game to display results
            //wintext.SetActive(true);    // Set wintext to active
            SceneManager.LoadScene("GoodEnding");

        }
        // if noise reaches 100, current time hits 6 o'clock, or pictures are at 0, you lose the game
        else if (noise.noiseRange == 100 || currentTime.hours == 6 || pics.pictures == 0 || wakeUpParents)
        {
            //Time.timeScale = 0;         // Stop time in game to display results

            if (wakeUpParents)
            {
                houseLights.enabled = true;   // Set wintext to active
                StartCoroutine(LoadEndingScene(4f));
            }
            else if (noise.noiseRange == 100 && audioSelected == false)
            {
                audioSelected = true;
                houseLights.enabled = true;   // Set wintext to active
                noiseEnd.clip = GetAudioClip(parentsAudioEnd);
                noiseEnd.Play();
                StartCoroutine(LoadEndingScene(4f));
            }
            else if(noise.noiseRange!=100)
            {
                SceneManager.LoadScene("Ending Patches");
            }
        }
	}

    IEnumerator LoadEndingScene(float delay)
    {
        Debug.Log("In Function");
        yield return new WaitForSeconds(delay);
        Debug.Log("In Function 2");
        SceneManager.LoadScene("Ending Patches");
    }
}
