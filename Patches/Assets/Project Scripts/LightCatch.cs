﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightCatch : MonoBehaviour
{
    public Light fl;
    public GameObject gameManager;

    private void OnTriggerEnter(Collider other)
    {
        if (fl.enabled == true)
        {
            //Debug.Log("can u see this");
            if (other.gameObject.tag == "ShyLight")
            {
                //Debug.Log("You've captured Elephant");
                if (gameManager.GetComponent<Enemy_Waypoints>())
                {
                    StartCoroutine(RunFromLight());
                    //Debug.Log("Elephant Before: " + gameManager.GetComponent<Enemy_Waypoints>().elephantInstance.transform.position);
                    gameManager.GetComponent<Enemy_Waypoints>().TeleportElephant();
                    //Debug.Log("Elepnat After: " + gameManager.GetComponent<Enemy_Waypoints>().elephantInstance.transform.position);
                    gameManager.GetComponent<Enemy_Waypoints>().elephant_timer = 900;
                    //Debug.Log("Eww, Light!");
                }
            }
            if (other.gameObject.tag == "LightFreeze")
            {
                //Debug.Log("You've captured Bear");
                if (gameManager.GetComponent<Enemy_Waypoints>())
                {
                    gameManager.GetComponent<Enemy_Waypoints>().freeze_bearOne = true;
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "LightFreeze")
        {
            //Debug.Log("You've lost Bear");
            if (gameManager.GetComponent<Enemy_Waypoints>())
            {
                gameManager.GetComponent<Enemy_Waypoints>().freeze_bearOne = false;
            }
        }
    }

    IEnumerator RunFromLight()
    {
        yield return new WaitForSeconds(2f);
    }
}

