﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayPause : MonoBehaviour {

    private Television television;  // To hold the Television script, used to get the current channel playing
    
    // Use this for initialization
    void Start () {
        television = GameObject.Find("polySurface15").GetComponent<Television>();   // Initialize the script into television
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter(Collision collision)
    {
        // if collider collides with right index finger, and also when it hasnt been clicked before
        if (collision.gameObject.tag == "RightIndex")// && !television.videoPlayer.isPlaying)
        {
            // if television.channel == 0, then start it at 1
            if (television.channel == 0)
            {
                television.channel = 1;
            }

            // if television.playmode == true, then set to false, else, set to true.
            if (television.playmode)
            {
                television.playmode = false;
            }
            else
            {
                television.playmode = true;
            }

            //Debug.Log(television.playmode);

            


            //Debug.Log("collided");
            ////if television current channel is greater than 1, then subtract current channel by 1, then set clickedLeft to true, so it wont keep subtracting 
            //if (television.channel ==  0)
            //{
            //    television.channel = 1;
            //    Debug.Log("Current Channel: " + television.channel);
            //}
            //else
            //{
            //    if(television.videoPlayer.isPlaying)
            //    {
            //        television.pause = true;
            //        //television.videoPlayer.Pause();
            //        //television.audioSource.Pause();
            //    }
            //    else if (!television.videoPlayer.isPlaying)
            //    {
            //        television.play true;
            //        //television.videoPlayer.Play();
            //        //television.audioSource.Play();
            //    }
            //}
        }
    }
}
