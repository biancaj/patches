﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TV_LeftButton : MonoBehaviour {

    private bool clickedLeft;       // To hold a boolean value, true when enter, false when exit

    private Television television;  // To hold the Television script, used to get the current channel playing

    // Use this for initialization
    void Start()
    {
        television = GameObject.Find("polySurface15").GetComponent<Television>();   // Initialize the script into television
        clickedLeft = false;                                                        // start off as left button hasn't been touched
    }
    
    private void OnCollisionEnter(Collision collision)
    {
        // if collider collides with right index finger, and also when it hasnt been clicked before
        if (collision.transform.tag == "RightIndex" && !clickedLeft)// && !television.videoPlayer.isPlaying)
        {
            //if television current channel is greater than 1, then subtract current channel by 1, then set clickedLeft to true, so it wont keep subtracting 
            if(television.channel > 1)
            {
                television.channel--;
                Debug.Log("Current Channel: " + television.channel);
                clickedLeft = true;
                //Debug.Log(clickedLeft);
            }
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        // if collider from finger leaves the button collider, then set clickedLeft to false.
        if (collision.transform.tag == "RightIndex" && clickedLeft)
        {
            clickedLeft = false;
            //Debug.Log(clickedLeft);
        }
    }
}
