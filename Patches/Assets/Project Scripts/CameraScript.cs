﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    public AudioSource camShutter;  // To hold an audio source, taking a picture
    public AudioSource camReload;   // To hold an audio source, reloading the camera
    public Light camFlash;          // To hold the light component used for cameras flash
    public int pictures;            // To hold the number of pictures in the camera
    public bool needsReload;        // To hold a bool value, true if camera needs to be reloaded, false when it doesn't

    // Use this for initialization
    void Start()
    {
        pictures = 10;              // Initialize the amount of total pictures at the start of the game by 10
        camFlash.enabled = false;   // Initialize the flashlight to start as disabled, will only be enabled for the flash
        needsReload = false;        // Initialize the bool as false, It starts off at no need to reload
    }

    // Update is called once per frame
    void Update()
    {
        // If left trigger button is pressed, and there are more than 0 pictures 
        if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger) && pictures > 0 && !needsReload && !camReload.isPlaying)
        {
            StartCoroutine(Flash());    // Start the coroutine to display the flash
            pictures--;                 // Decrease the number of remaining pictures by 1 
            needsReload = true;         // After taking a picture, camera needs to be reloaded
        }
    }
    /// <summary>
    /// Courtine function, when camera takes a picture, activate flash for 1 sec
    /// </summary>
    /// <returns></returns>
    IEnumerator Flash()   //coroutine used for flash
    {
        camFlash.enabled=true;                  // Enable the camera flash
        camShutter.Play();                      // Play audio to camera shutter
        yield return  new WaitForSeconds(1f);   // Wait for one second, yield function
        camFlash.enabled=false;                 // Disable the camera flash
    }
}
