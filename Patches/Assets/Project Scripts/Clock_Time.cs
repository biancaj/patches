﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Clock_Time : MonoBehaviour {

    public GameObject grandfatherClock; //object with the audiosource attached, on grandfather clock
    public AudioClip grandfatherClockNoise; //the clock noise
    public Text clock_time;         // To hold the textbox within the timer
    public float hours;    // To hold the hours and seconds
    private float interval = 1f;
    private float startTime = 1f;
    public int hourInterval;

    // Use this for initialization
    void Start()
    {
        clock_time = GetComponent<Text>() as Text;  // Retrieve the text component within the timer object
        hourInterval = 0;
    }

    // Update is called once per frame
    void Update()
    {
        /* NOTE: 
            To test timer, you can increase delta time within the following steps:
            Edit -> Project Settings -> Time: In the settings find Delta Time, It's originally at 1, but can be increased to 100 max. (increases in game time)
         */
        hours = (int)(((Time.timeSinceLevelLoad / 60)) / 1.34f) + hourInterval ;    // Updates by Time.time/60 to get the minutes, also, % 60 to keep clock in standard time
        if (hours == startTime)
        {
            grandfatherClock.GetComponent<AudioSource>().PlayOneShot(grandfatherClockNoise);
            startTime += interval;
        }
        if (hours == 0)
        {
            int display = 12;
            clock_time.text = (display.ToString() + ":00");    // Display the current hour and minute to the tet field	
        }
        else
        {
            clock_time.text = (hours.ToString() + ":00");    // Display the current hour and minute to the tet field	
        }

    }
}
